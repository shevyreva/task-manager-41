package ru.t1.shevyreva.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.endpoint.ISystemEndpoint;
import ru.t1.shevyreva.tm.api.service.ICommandService;
import ru.t1.shevyreva.tm.api.service.IPropertyService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @Nullable
    protected ICommandService getCommandService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getCommandService();
    }

    @Nullable
    public IPropertyService getPropertyService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getPropertyService();
    }

    @Nullable
    public ISystemEndpoint getSystemEndpoint() {
        return getServiceLocator().getSystemEndpoint();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
